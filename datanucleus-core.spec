%global debug_package %nil
%global commit f7d15412f9a95353ecfbc0a72211568fd2efe21a
%global shortcommit %(c=%{commit}; echo ${c:0:7})
Name:                datanucleus-core
Version:             3.2.15
Release:             4
Summary:             DataNucleus Core
License:             ASL 2.0
URL:                 http://www.datanucleus.org/
Source:              https://github.com/datanucleus/datanucleus-core/archive/%{commit}/datanucleus-core-%{commit}.tar.gz
BuildRequires:       java-devel mvn(javax.time:time-api) mvn(javax.jdo:jdo-api)
BuildRequires:       mvn(javax.transaction:jta) mvn(log4j:log4j) mvn(mx4j:mx4j) mvn(mx4j:mx4j-tools)
BuildRequires:       mvn(org.apache.ant:ant)
BuildRequires:       mvn(org.apache.geronimo.specs:geronimo-validation_1.0_spec)
BuildRequires:       mvn(org.ow2.asm:asm) mvn(org.eclipse.osgi:org.eclipse.osgi)
BuildRequires:       mvn(junit:junit) maven-local maven-install-plugin
BuildRequires:       mvn(biz.source_code:base64coder)
BuildRequires:       datanucleus-maven-parent
BuildArch:           noarch

%description
DataNucleus Core provides the primary components
of a heterogeneous Java persistence solution.
It supports persistence API's being layered on
top of the core functionality.

%package javadoc
Summary:             Javadoc for %{name}

%description javadoc
This package contains javadoc for %{name}.

%prep
%setup -q -n %{name}-%{commit}
rm src/java/org/datanucleus/plugin/EclipsePluginRegistry.java
%pom_remove_dep org.eclipse.core:org.eclipse.core.runtime.compatibility.auth
%pom_remove_dep org.eclipse.equinox:org.eclipse.equinox.registry
%pom_remove_dep org.eclipse.equinox:org.eclipse.equinox.common
%pom_remove_dep org.eclipse.equinox:org.eclipse.equinox.preferences
%pom_remove_dep org.eclipse.core:org.eclipse.core.runtime
%pom_remove_dep org.eclipse.core:org.eclipse.core.contenttype
%pom_remove_dep org.eclipse.core:org.eclipse.core.jobs
%pom_remove_dep javax.management:jmx
rm -r src/java/org/datanucleus/asm
%pom_add_dep org.ow2.asm:asm
sed -i "s|org.datanucleus.asm|org.objectweb.asm|" $(find . -name "*.java")
%pom_xpath_inject "pom:project/pom:build/pom:plugins/pom:plugin[pom:artifactId = 'maven-bundle-plugin' ]/pom:configuration/pom:instructions" '
<Require-Bundle>org.eclipse.equinox.registry;resolution:=optional,org.eclipse.core.runtime;resolution:=optional</Require-Bundle>
<Bundle-Name>${project.name}</Bundle-Name>
<Bundle-Vendor>DataNucleus</Bundle-Vendor>
<Premain-Class>org.datanucleus.enhancer.DataNucleusClassFileTransformer</Premain-Class>
<Export-Package>org.datanucleus*;version="${project.version}"</Export-Package>'
%pom_xpath_inject "pom:project/pom:build/pom:plugins/pom:plugin[pom:artifactId = 'maven-bundle-plugin' ]" "
<executions>
  <execution>
    <id>bundle-manifest</id>
    <phase>process-classes</phase>
    <goals>
      <goal>manifest</goal>
    </goals>
  </execution>
</executions>"
%pom_remove_dep javax.cache:cache-api
find -name 'JavaxCache*.java' -delete
sed -i".orig" "/javax.cache/d" plugin.xml
find -name 'Base64.java' -delete
sed -i "s|org.datanucleus.util.Base64|biz.source_code.base64Coder.Base64Coder|" $(find . -name "*.java")
sed -i "s|\.Base64;|\.Base64Coder;|" $(find . -name "*.java")
sed -i "s|Base64\.|Base64Coder\.|" $(find . -name "*.java")
%pom_add_dep biz.source_code:base64coder
sed -i 's/\r//' META-INF/LICENSE.txt META-INF/NOTICE.txt META-INF/README.txt
cp -p META-INF/LICENSE.txt .
cp -p META-INF/NOTICE.txt .
cp -p META-INF/README.txt .

%build
%mvn_build -f

%install
%mvn_install

%files -f .mfiles
%doc LICENSE.txt NOTICE.txt

%files javadoc -f .mfiles-javadoc
%doc LICENSE.txt NOTICE.txt

%changelog
* Wed Dec 29 2021 yaoxin <yaoxin30@huawei.com> - 3.2.15-4
- This package depends on log4j.After the log4j vulnerability CVE-2021-44832 is fixed,the version needs to be rebuild.

* Fri Dec 24 2021 yaoxin <yaoxin30@huawei.com> - 3.2.15-3
- This package depends on log4j.After the log4j vulnerability CVE-2021-45105 is fixed,the version needs to be rebuild.

* Thu Dec 16 2021 wangkai <wangkai385@huawei.com> - 3.2.15-2
- This package depends on log4j.After the log4j vulnerability CVE-2021-44228 is fixed,the version needs to be rebuild.

* Fri Aug 14 2020 leiju <leiju4@huawei.com> - 3.2.15-1
- Package init
